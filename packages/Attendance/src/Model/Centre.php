<?php

namespace Attendance\Model;

use Illuminate\Database\Eloquent\Model;

class Centre extends Model{
    protected $table = 'centre';
    protected $fillable = [];
    protected $primaryKey = 'pk';
}   