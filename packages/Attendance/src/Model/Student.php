<?php

namespace Attendance\Model;

use Illuminate\Database\Eloquent\Model;

class Student extends Model{
    protected $table = 'student';
    protected $fillable = [];
    public $paperCode;
    public function getViewAttendanceAttribute(){
       
        return count($this->view_attendance) > 0 ? 'p' : 'a'; 
    }
    public function viewAttendance(){
        
        return $this->belongsTo(ViewAttendance::class, 'examno',  'examno');
    }
}   