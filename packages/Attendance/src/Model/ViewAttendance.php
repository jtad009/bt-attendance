<?php

namespace Attendance\Model;

use Illuminate\Database\Eloquent\Model;

class ViewAttendance extends Model
{
    protected $table = 'view_attendance';
    protected $fillable = [];
    // protected $with = ['students'];
    public function students()
    {
        return $this->hasOne(Student::class, 'examno', 'examno');
    }
}
