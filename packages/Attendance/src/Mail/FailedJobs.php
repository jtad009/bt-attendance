<?php

namespace Attendance\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\Events\JobFailed;
use Illuminate\Queue\SerializesModels;

class FailedJobs extends Mailable
{
    use Queueable, SerializesModels;
    private $jobInformation;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(JobFailed $event)
    {
        $this->jobInformation = $event;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        dump($this->jobInformation->exception->getMessage());
        return [];
        // $this->from('development@botosoft.net', 'Attendance Failed Job Notification')
        // ->to(env('ADMIN_EMAIL', 'development@botosoft.net'))
        // ->view('attendance::emails.jobs.failed')
        // ->with([
        //     'JObDailed'=>'Hi Ther e',
        //     // 'jobClass'=> $this->jobInformation->job->resolveName(),
        //     // 'message'=> $this->event->event->exception->getMessage(),
        //     'jobBody'=>$this->event->event->job->getRawBody(),
        //     // 'Exception' => $this->event->event->exception->getTraceAsString(),
        // ]);
    }
}
