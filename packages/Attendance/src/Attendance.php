<?php

namespace Attendance;

use Attendance\Model\Centre;
use Attendance\Model\Paper;
use Attendance\Model\State;
use Attendance\Model\Student;
use Attendance\Model\Subject;
use Attendance\Model\ViewAttendance;
use Illuminate\Support\Facades\Storage;

class Attendance
{
    public function getCenter($stateCode)
    {
        return Centre::where('state', $stateCode)
            ->select('name', 'code', 'state', 'pk');
    }
    public function getPaper()
    {
        return  Paper::orderBy('name', 'ASC');
    }
    public function getSubjectId($subjectCode): array
    {
        info('Detals ', [$subjectCode]);
        return Subject::where('code', $subjectCode)
        ->select('pk', 'description')
        ->first()
        ->toArray();
    }
    public function getStudents($centerCode, $subjectId, $paperCode): array
    {
        info('Detals ', [$centerCode, $subjectId, $paperCode]);
        // dump($centerCode);
        return  Student::select('surname', 'firstname', 'middlename', 'examno')
        ->whereRaw("fkcentre =  '{$centerCode}' and 
        (fksubject1 = {$subjectId} OR 
        fksubject2 = {$subjectId} OR 
        fksubject3 = {$subjectId} OR 
        fksubject4 = {$subjectId} OR 
        fksubject5 = {$subjectId} OR 
        fksubject6 = {$subjectId} OR 
        fksubject7 = {$subjectId} OR 
        fksubject8 = {$subjectId} OR 
        fksubject9 = {$subjectId}) ")
            ->with('viewAttendance', function ($q) use ($paperCode) {
                return $q
                ->selectRaw("(CASE WHEN examno then 'Present' ELSE 'Absent' END) as attendance, examno, papercode")
                ->where('papercode', $paperCode);
            })
            ->get()
            ->toArray();
    }

    public function c($centerCode, $paperCode, $examno): array
    {

        return ViewAttendance::whereRaw("centrecode like  '{$centerCode}%' and papercode = {$paperCode} and examno = {$examno}")
            ->get()->toArray();
    }

    public function getState($code): State
    {
        return State::where('code', $code)->orWhere('name', $code)->get();
    }
    /**
     * Write state log to the lcog file
     * @param String $stateFolder the state to write log for
     * @param Array $options the options to update in the log file
     * @return void 
     */
    public function applicationLogs($stateFolder, $options = []): void
    {
        $logFileName =  DIRECTORY_SEPARATOR."logs".DIRECTORY_SEPARATOR."{$stateFolder}.txt";
        if (Storage::exists($logFileName)) :
            $stateData = json_decode(Storage::get($logFileName), true);
            // info('file contents',[$stateData, $options]);
            if (count($options) > 0) :
                $stateData['papers'] =  $stateData['papers'] + $options['paper'];
                $stateData['centers'] =  $stateData['centers'] + $options['centers'];
                $stateData['failures'] =  $stateData['failures'] + $options['failures'];
                Storage::put(
                    $logFileName,
                    json_encode($stateData)
                );
            endif;
        else :
            Storage::put(
                $logFileName,
                json_encode([
                    'papers' => 0,
                    'centers' => 0,
                    'failures' => 0
                ])
            );
        endif;
    }
    /**
     * Check state log file to see if it has errors
     * @param String $stateData the stateLog file to read
     * @param $String $keyToFind the information about state you want  options [  papers, center, failures     ]
     * @return int the count for each key for the state
     */
    public function checkIfStateHasErrors($stateFolder, $keyToFind = 'failures'): int
    {
        $logFileName = DIRECTORY_SEPARATOR."logs".DIRECTORY_SEPARATOR."{$stateFolder}.txt";
        if (Storage::exists($logFileName)) :
            $stateData = json_decode(Storage::get($logFileName), true);
            return $stateData[$keyToFind];
        endif;
        return 0;
    }
}
