<?php

namespace Attendance\Http\Controllers;

use App\Http\Controllers\Controller;
use Attendance\Jobs\MakeAttendanceJob;
use Attendance\Model\State;
use Illuminate\Http\Request;

class AttendanceController extends Controller
{
    /**
     * INDEX ROUTE 
     * @param Request $request
     */
    public function index(Request $request)
    {
        $states = State::get()->pluck('name', 'name');
        return view('attendance::index', ['state' => $states]);
    }
    /**
     * Get the state documents
     * @param Request $request
     */
    public function getStateDocuments(Request $request)
    {
        // dd($request->input('state'));
        $state = State::whereIn('name', [$request->input('state')])->get()->toArray();


        if (count($state) > 0) :
            dispatch(new MakeAttendanceJob($state));
        endif;
        $expectedTotalPapers = Attendance()->getPaper()->count();
        $expectedTotalCenter = Attendance()->getCenter($request->input('state'))->count();
        return response()->json([
            'response' => 'completed successfully',
            'expectedTotalCenter' =>  $expectedTotalCenter / $expectedTotalPapers,
            'expectedTotalPapers' => $expectedTotalPapers,
            'totalExpected' => $expectedTotalPapers
        ]);
    }
    /**
     * Get the progress of the job and send to the UI
     * Request Request $request
     */
    public function getProgress(Request $request)
    {
        $totalCenters = Attendance()->checkIfStateHasErrors(
            $request->query('state'),
            'centers'
        );

        $totalPapers = Attendance()->checkIfStateHasErrors(
            $request->query('state'),
            'papers'
        );

        return  response()->json([
            'progress' => (($totalCenters != 0 ?  $totalCenters :  1) / ($totalPapers != 0 ? $totalPapers : 1)),
            'totalPapers' => $totalCenters . '/' . $totalPapers,
            'totalCenters' => $totalCenters
        ]);
    }
}
