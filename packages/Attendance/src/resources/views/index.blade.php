@extends('attendance::layout.mainlayout')
@section('content')
<!-- <div class="container border min-height"> -->

{{ Form::open(array('action' => 'AttendanceController@getStateDocuments', 'class'=>"form-signin", 'id'=>'stateForm')) }}
<div class="alert alert-success d-none">
   
</div>
<h1 class="h3 mb-3 font-weight-normal">SELECT A STATE</h1>

<?php echo Form::select('state', $state, '', ['class' => "form-control shadow-sm", 'required', 'id'=>'state']); ?>
<div class="progress mt-3">
    <div class="progress-bar" role="progressbar" style="width: 0%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
</div>
<?php echo Form::submit('Generate PDF', ["class" => "btn btn-lg btn-primary btn-block mt-3 shadow-sm"]); ?>
{{ Form::close() }}

@include('attendance::layout.partials.script')
<!-- </div> -->
@endsection
