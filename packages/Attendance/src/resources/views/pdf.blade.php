<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Waec Attendance</title>
    <style>
        table,
        td,
        th {
            border: 1px solid black;
        }

        table {
            width: 100%;
            border-collapse: collapse;
        }

        .text-center {
            text-align: center;
        }

        .font-14 {
            font-size: 16px;
        }

        .font-12 {
            font-size: 14px;
        }
    </style>
</head>

<body>
    <h2 class="text-center mb-3">CIVAMPEMS REPORT {{ env('EXAM_YEAR') }}</h2>
    <h2 class="text-center font-14">Attendance report for {{ $center }}, {{ $state }}</h2>
    <h2 class="text-center font-12"> {{ $paper }}</h2>
    <table class="table">
        <thead>

            <tr class="table-danger">
                <th height="20" colspan="1" style="width: 10px; text-align:center; font-weight: bold; font-size: 15px">S/No</th>
                <th height="20" colspan="1" style="width: 26px; text-align:center; font-weight: bold; font-size: 15px">Examination No</th>
                <th height="20" colspan="1" style="width: 42px; text-align:center; font-weight: bold; font-size: 15px">Candidate Name</th>
                <th height="20" colspan="1" style="width: 23px; text-align:center; font-weight: bold; font-size: 15px">Attendance</th>

            </tr>
        </thead>
        <tbody>
            <?php $i = 0; ?>
            @foreach($student as $key => $val)
            <?php $i++; ?>
            <tr>
                <td colspan="1" style="width: 10px; text-align:center">{{$i}}</td>
                <td colspan="1" style="width: 26px; text-align:center">{{$val['examno']}}</td>
                <td colspan="1" style="width: 42px; text-align:center; vertical-align: middle;height: 20px;">{{ $val['surname']. ''.$val['firstname'] }}</td>
                <td colspan="1" style="width: 23px; text-align:center">{{ !is_null($val['view_attendance']) ? $val['view_attendance']['attendance'] : 'Absent'}}</td>
            </tr>


            @endforeach

        </tbody>
    </table>


</body>

</html>