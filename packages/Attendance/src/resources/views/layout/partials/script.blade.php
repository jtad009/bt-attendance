<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
<script>
   $(document).ready(function() {
      var timer;
      var maxVal = document.querySelector('.progress-bar');
      $(document).on('submit', 'form.form-signin', function(e) {
         e.preventDefault();
         console.log("Form Submitted", $(this).serialize());

         jQuery.ajax({
            headers: {
               'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            },
            url: "{{ url('attendance/getStateDetails') }}",
            method: 'post',
            data: $(this).serialize(),
            success: function(result) {
              
               maxVal.setAttribute('maxVal', result.totalExpected);
               console.log(result);
               timer = getProgress($("#state option:selected").text(), )
               $('div.alert').show();
               $('div.alert').html("Done Successful ");
            },
            error: function(err) {
               console.error(err);
            }
         });

      });

      function getProgress(state, times) {
         var interval = setInterval(() => {
               jQuery.ajax({

                  url: "{{ url('attendance/getDownloadState') }}",
                  method: 'get',
                  data: {
                     'state': state
                  },
                  success: function(result) {
                    var progress = parseInt(parseInt(result.progress) / parseInt(maxVal.getAttribute('maxVal')) * 100);
                     maxVal.style.width = progress +'%';
                     console.log(progress +'%');
                     if(progress == 100){
                        window.clearInterval(interval);
                     }
                  },
                  error: function(err) {
                     console.error(err);
                  }
               })
            },
            5000);
      }
   });
</script>