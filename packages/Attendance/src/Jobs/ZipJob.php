<?php

namespace Attendance\Jobs;

use App\Jobs\Job;
use Illuminate\Http\Request;
use File;
use Illuminate\Support\Facades\File as FacadesFile;
use Illuminate\Support\Facades\Storage;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use ZanySoft\Zip\Zip;
use ZipArchive;

class ZipJob extends Job
{
    private $stateName;
    public function __construct(String $stateName)
    {
        $this->stateName = $stateName;
    }
    public function handle()
    {

        $zip = new ZipArchive();
        if (strlen($this->stateName) > 0) :
            if (Attendance()->checkIfStateHasErrors($this->stateName) <= 0) :
                info('Zip Job Starting for ', [$this->stateName]);
                $fileName = "{$this->stateName}";
         
                $folder = storage_path('app'.DIRECTORY_SEPARATOR.'state_data'.DIRECTORY_SEPARATOR . $fileName);
                if ($zip->open($folder . '.zip', ZipArchive::CREATE | ZipArchive::OVERWRITE) === TRUE) {
                    $files = new RecursiveIteratorIterator(
                        new RecursiveDirectoryIterator($folder),
                        RecursiveIteratorIterator::LEAVES_ONLY
                    );
                    foreach ($files as $name => $file) :
                        // Skip directories (they would be added automatically)
                        if (!$file->isDir()) :
                            // Get real and relative path for current file
                            $filePath = $file->getRealPath();
                            $relativePath = substr($filePath, strlen($folder) + 1);

                            // Add current file to archive
                            $zip->addFile($filePath, $relativePath);
                        endif;
                    endforeach;
                    $zip->close();
                }
                info('Zip Job Finished for ', [$this->stateName]);
                $zipPath = storage_path("app".DIRECTORY_SEPARATOR."zips".DIRECTORY_SEPARATOR);
                try {
                    if (!file_exists($zipPath)) :
                        mkdir($zipPath);
                    endif;
                    rename($folder . '.zip', storage_path("app/zips/${fileName}") . '.zip');
                } catch (\Throwable $th) {
                    info('Info: ', [$th->getMessage()]);
                }
            endif;

        endif;
    }
}
