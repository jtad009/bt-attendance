<?php

namespace Attendance\Jobs;

use App\Jobs\Job;
use Attendance\Model\Centre;
use Illuminate\Support\Facades\Storage;

class PaperJob extends Job 
{
    private $stateCode;
    private $stateName;
    private $paperCode;
    private $paperTitle;
    private $filePath;
    // public $tries = 5;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(String $stateCode, String $stateName, $filePath, $paperCode, $paperTitle)
    {
        $this->stateCode = $stateCode;
        $this->stateName = $stateName;
        $this->paperCode = $paperCode;
        $this->paperTitle = $paperTitle;
        $this->filePath = $filePath;
    }
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $centerInformation = Attendance()
        ->getCenter($this->stateName)
        ->get();


        $centers = $centerInformation->pluck('name', 'code');   
        info('centers', [$centers]);   
       
        if (count($centers)) :
            for ( $i = 0; $i < count($centers); $i++) :
               
                $store_path = $this->filePath;
                info('Center information', [$centerInformation[$i]]);
                info('Center information', [$this->stateName, $this->stateCode, $this->paperCode, $this->paperTitle]);
                dispatch(new FetchCenterJob(
                    $this->stateName,
                    $this->stateCode,
                    $store_path,
                    $this->paperCode,
                    $this->paperTitle,
                    $centerInformation[$i]
                ));
                

                if ($i == 3) {
                    break;
                }
            endfor;
        endif;
    }
    /**
     * Handle a job failure.
     *
     * @param  \Throwable  $exception
     * @return void
     */
    public function failed(\Throwable $event)
    {
        $fileName = '/logs/job.txt';
       info('Papaer ', [$event->getMessage()]);
    }

   
}
