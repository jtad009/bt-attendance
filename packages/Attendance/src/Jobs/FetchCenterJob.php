<?php

namespace Attendance\Jobs;

use App\Jobs\Job;
use Illuminate\Support\Facades\Storage;
use Throwable;

class FetchCenterJob extends Job
{
    private $stateCode;
    private $filePath;
    private $paperCode;
    private $paper;
    private $state;
    private $centerInfo;

    // public $tries = 5;
    /**
     * Constructor
     * @param String $stateName
     * @param String $stateCode
     * @param String $filePath
     * @param String $paperCode
     * @param String $paper
     * @param String $center
     * @param String $centerCode
     * 
     * @return Null
     */
    public function __construct(
        String $stateName,
        String $stateCode,
        String $filePath,
        String $paperCode,
        String $paper,
        $centerInformation
    ) {
        $this->stateCode = $stateCode;
        $this->filePath = $filePath;
        $this->paperCode = $paperCode;
        $this->paper = $paper;
        $this->state = $stateName;
        $this->centerInfo = $centerInformation;
    }
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
       try {
        $subjectId = Attendance()->getSubjectId(substr($this->paperCode, 0, 3))['pk'];
        $student = Attendance()->getStudents($this->centerInfo['pk'], $subjectId, $this->paperCode);
        $pdf =  \PDF::loadView('attendance::pdf', [
            'student' => $student,
            'paper' => $this->paper,
            'center' => $this->centerInfo['name'],
            'state' => $this->state,
        ]);
        
        Storage
        ::disk('local')
        ->put("{$this->filePath}".DIRECTORY_SEPARATOR."{$this->centerInfo['code']}_{$this->centerInfo['name']}.pdf", $pdf->output());
       } catch (\Throwable $th) {
          $this->fail($th);
       }
        
    }
    /**
     * Handle a job failure.
     *
     * @param  \Throwable  $exception
     * @return void
     */
    public function failed(Throwable $event)
    {
        
        $fileName = '/logs/job.txt';
        info('Called Failing', [$event]);
    }

    public function uniqueId(){
        //return $this->state;
    }
}
