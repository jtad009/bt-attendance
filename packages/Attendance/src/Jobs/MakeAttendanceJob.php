<?php

namespace Attendance\Jobs;

use App\Jobs\Job;
use Throwable;

// use PDF;

class MakeAttendanceJob extends Job
{
    private $state;
    

    // public $tries = 5;
    /**
     * Constructor
     * @param String $stateName
     * 
     * @return Null
     */
    public function __construct(
        $state
    ) {
        $this->state = $state;
    }
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $papers = Attendance()->getPaper()->get()->pluck('code', 'name');
        $j = 0;
        while($j < count($this->state)):
        
        $stateFolder = $this->state[$j]['name'];
        // Attendance()->applicationLogs($stateFolder, []);
        $i = 0;
        // dump($stateFolder);
        if (count($papers)) :
            foreach ($papers as $paperTitle => $paperCode) :
                $centreName = str_replace(['/', ' ', '-', '.'], '_', $paperTitle);
                $store_path = 'state_data'.DIRECTORY_SEPARATOR.$stateFolder . DIRECTORY_SEPARATOR . $centreName . '_' . $paperCode;
                dispatch(new PaperJob(
                    $this->state[$j]['code'],
                    $this->state[$j]['name'],
                    $store_path,
                    $paperCode,
                    $paperTitle
                )
            );
                
                $i++;
                if ($i == 2) {
                    break;
                }
            endforeach;
        endif;
        $j++;
    endwhile;
    }
    /**
     * Handle a job failure.
     *
     * @param  \Throwable  $exception
     * @return void
     */
    public function failed(Throwable $event)
    {
        
        $fileName = '/logs/job.txt';
        info('Called Failing', [$event]);
    }

    
}
