<?php

namespace Attendance\Providers;

use Attendance\Facades\Attendance;
use Attendance\Jobs\ZipJob;
use Illuminate\Support\ServiceProvider;
use Illuminate\Foundation\AliasLoader;
use Attendance\Mail\FailedJobs;

use Illuminate\Queue\Events\JobFailed;
use Illuminate\Queue\Events\JobProcessed;
use Illuminate\Queue\Events\JobProcessing;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\Facades\Storage;


/**
 * Attendance service provider
 *
 * @author   Israel Edet <israel.edet@botosoft.net>
 */
class AttendanceServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        include __DIR__ . '/../Http/FacadeHelper/Helper.php';
        $this->publishes([
            __DIR__ . '/../config/attendance.php' => config_path('attendance.php'),
            __DIR__ . '/../resources/assets/bootstrap.css' => public_path('assets/css/bootstrap.css'),
            __DIR__ . '/../resources/assets/js/script.js' => public_path('assets/js/script.js'),
        ], 'attendance');


        $this->app->register(AttendanceRouteServiceProvider::class);


        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang', 'attendance');

        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'attendance');
        $this->handleFailedJobs();
        $this->handleJobProgress();
        
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

        $this->registerFacades();
    }

    /**
     * Register Bouncer as a singleton.
     *
     * @return void
     */
    protected function registerFacades()
    {
        $loader = AliasLoader::getInstance();
        $loader->alias('Attendance', Attendance::class);

        $this->app->singleton('Attendance', function () {
            return app()->make(Attendance::class);
        });
    }

    /**
     * Register the console commands of this package
     *
     * @return void
     */
    protected function registerCommands(): void
    {
        if ($this->app->runningInConsole()) {
            $this->commands([]);
        }
    }

    protected function handleFailedJobs(): void
    {
        Queue::failing(function (JobFailed $event) {

            $fileName = '/logs/failed_job.txt';
            $currentState = trim(str_replace(
                ['\/', ';s', '"'],
                ' ',
                explode(':', $event->job->payload()['data']['command'])[24]
            ));
            
            Attendance()->applicationLogs($currentState, [
                'centers' => 0,
                'failures' => 1,
                'paper' => 0
            ]);
            // CREATE A FILE AND SEND IT TO THE ADMIN
            // $this->logJobs($fileName, $currentState);

            // Mail::to(env('ADMIN_EMAIL'))->send(new FailedJobs($event));
            // Notification::route('slack', $slackUrl)->notify(new SlackFailedJob($event));
        });
    }

    protected function handleJobProgress(): void
    {
        Queue::before(function (JobProcessing $event) {
            $fileName = '/logs/jobs.txt';
            info('Queue', [$event->job->getQueue()]);
            if ($event->job->resolveName() === 'Attendance\Jobs\PaperJob') :
                $currentState =  trim(str_replace(['\/', ';s', '"'], ' ', explode(':', $event->job->payload()['data']['command'])[12]));
                
                Attendance()->applicationLogs($currentState, [
                    'centers' => 0,
                    'failures' => 0,
                    'paper' => 1
                ]);
            endif;
            if ($event->job->resolveName() === 'Attendance\Jobs\FetchCenterJob') :
                $currentState =  trim(str_replace(['\/', ';s', '"'], ' ', explode(':', $event->job->payload()['data']['command'])[24]));
                Attendance()->applicationLogs($currentState, [
                    'centers' => 1,
                    'failures' => 0,
                    'paper' => 0
                ]);
            endif;
        });
        Queue::after(function (JobProcessed $event) {
            
            if ($event->job->resolveName() === 'Attendance\Jobs\FetchCenterJob') :
                $currentState =  trim(str_replace(['\/', ';s', '"'], ' ', explode(':', $event->job->payload()['data']['command'])[24]));
            endif;
            if(Queue::size() == 0 &&  ($event->job->resolveName() !== 'Attendance\Jobs\ZipJob')):
                dispatch(new ZipJob($currentState));
            endif;
            info('Queue Size:', [Queue::size(), ]);
        });
    }

    private function logJobs($fileName, $event): void
    {
        if (Storage::exists($fileName)) :
            Storage::append(
                $fileName,
                json_encode(
                    [
                        'payload' =>  trim(str_replace(
                            ['\/', ';s', '"'],
                            ' ',
                            explode(':', $event->job->payload()['data']['command'])[24]
                        )),
                    ]
                )
            );
        else :
            Storage::put(
                $fileName,
                json_encode([

                    'payload' =>  trim(str_replace(
                        ['\/', ';s', '"'],
                        ' ',
                        explode(':', $event->job->payload()['data']['command'])[24]
                    )),
                ])
            );
        endif;
    }
}
